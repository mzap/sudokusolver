package pl.mzap.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class BoardTest {

    private Board board;

    @BeforeEach
    void setUp() {
        Byte[][] initBoard = {
                {null, null, null, 2, 6, null, 7, null, 1},
                {6, 8, null, null, 7, null, null, 9, null},
                {1, 9, null, null, null, 4, 5, null, null},
                {8, 2, null, 1, null, null, null, 4, null},
                {null, null, 4, 6, null, 2, 9, null, null},
                {null, 5, null, null, null, 3, null, 2, 8},
                {null, null, 9, 3, null, null, null, 7, 4},
                {null, 4, null, null, 5, null, null, 3, 6},
                {7, null, 3, null, 1, 8, null, null, null},
        };
        board = new Board(initBoard, null);
    }

    @RepeatedTest(10)
    void should_return_true_if_solved() {
        long startTime = System.currentTimeMillis();
        boolean isSolved = board.calculate();
        long stopTime = System.currentTimeMillis();
        long calculatedTime = stopTime - startTime;
        System.out.println("Solved in: " + calculatedTime + " ms");
        Assertions.assertTrue(isSolved);
    }

    @ParameterizedTest
    @ValueSource(bytes = {6, 1, 8, 7})
    void should_return_true_if_predicated_number_is_in_grid_column(byte predicatedNumber) {
        boolean isInGridRow = board.isInGridColumn(predicatedNumber, 0);
        Assertions.assertTrue(isInGridRow);
    }

    @ParameterizedTest
    @ValueSource(bytes = {2, 3, 4, 5, 9})
    void should_return_false_if_predicated_number_is_not_in_grid_column(byte predicatedNumber) {
        boolean isInGridRow = board.isInGridColumn(predicatedNumber, 0);
        Assertions.assertFalse(isInGridRow);
    }

    @ParameterizedTest
    @ValueSource(bytes = {2, 6, 7, 1})
    void should_return_true_if_predicated_number_is_in_grid_row(byte predicatedNumber) {
        boolean isInGridColumn = board.isInGridRow(predicatedNumber, 0);
        Assertions.assertTrue(isInGridColumn);
    }

    @ParameterizedTest
    @ValueSource(bytes = {3, 4, 5, 8, 9})
    void should_return_false_if_predicated_number_is_not_in_grid_row(byte predicatedNumber) {
        boolean isInGridColumn = board.isInGridRow(predicatedNumber, 0);
        Assertions.assertFalse(isInGridColumn);
    }

    @ParameterizedTest
    @ValueSource(bytes = {1, 9, 6, 8})
    void should_return_true_if_predicated_number_is_in_current_segment(byte predicatedNumber) {
        boolean isInSegment = board.isInSegment(predicatedNumber, BoardSegment.TOP_LEFT);
        Assertions.assertTrue(isInSegment);
    }

    @ParameterizedTest
    @ValueSource(bytes = {2, 3, 4, 5, 7})
    void should_return_false_if_predicated_number_is_not_in_current_segment(byte predicatedNumber) {
        boolean isInSegment = board.isInSegment(predicatedNumber, BoardSegment.TOP_LEFT);
        Assertions.assertFalse(isInSegment);
    }

}
