module pl.mzap {
    requires javafx.controls;
    requires javafx.fxml;

    opens pl.mzap.controller to javafx.fxml;
    exports pl.mzap;
    exports pl.mzap.controller;
}