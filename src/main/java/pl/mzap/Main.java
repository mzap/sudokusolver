package pl.mzap;

import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import pl.mzap.controller.SudokuController;
import pl.mzap.model.Board;
import pl.mzap.model.BoardView;

import java.io.IOException;

public class Main extends Application {

    private Board board;
    private BoardView boardView;

    @Override
    public void start(Stage stage) throws Exception {
        BorderPane borderPane = buildView();
        Scene scene = initializeScene(borderPane);
        setStage(stage, scene);
        initializeBoardView(borderPane);
        initializeBoard();
    }

    private BorderPane buildView() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/Sudoku.fxml"));
        BorderPane borderPane = fxmlLoader.load();
        SudokuController controller = fxmlLoader.getController();
        controller.setMain(this);
        return borderPane;
    }

    private Scene initializeScene(BorderPane borderPane) {
        Scene scene = new Scene(borderPane);
        scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
        return scene;
    }

    private void setStage(Stage stage, Scene scene) {
        stage.setScene(scene);
        stage.setTitle(Settings.APP_NAME);
        stage.show();
    }

    private void initializeBoardView(BorderPane borderPane) {
        boardView = new BoardView((GridPane) borderPane.getCenter());
    }

    private void initializeBoard() {
        board = new Board(Settings.INIT_BOARD, boardView);
    }


    public void calculate() {
        board.clearBoard();
        Task<Boolean> task = new Task<>() {
            @Override
            protected Boolean call() {
                return board.calculate();
            }
        };
        Thread calculateThread = new Thread(task);
        calculateThread.setDaemon(true);
        calculateThread.start();
    }

    public static void main(String[] args) {
        launch();
    }

}
