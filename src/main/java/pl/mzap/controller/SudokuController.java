package pl.mzap.controller;

import javafx.fxml.Initializable;
import pl.mzap.Main;

import java.net.URL;
import java.util.ResourceBundle;

public class SudokuController implements Initializable {

    private Main main;

    public void setMain(Main main) {
        this.main = main;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    public void onCalculateAction() {
        main.calculate();
    }
}
