package pl.mzap.model;

import javafx.scene.layout.GridPane;
import pl.mzap.Settings;

import java.util.Objects;

public class BoardView {

    private final Cell[][] board = new Cell[Settings.BOARD_SIZE][Settings.BOARD_SIZE];

    public BoardView(GridPane gridPane) {
        for (byte row = 0; row < Settings.BOARD_SIZE; row++) {
            for (int column = 0; column < Settings.BOARD_SIZE; column++) {
                Byte number = Settings.INIT_BOARD[row][column];
                Cell cell = new Cell(number, row, column);
                board[row][column] = cell;
                gridPane.add(cell, column, row);
            }
        }
    }

    public void updateCell(Byte number, int row, int column) {
        Cell cell = board[row][column];
        cell.setNumber(number);
    }

    public void clearBoardView() {
        for (byte row = 0; row < Settings.BOARD_SIZE; row++) {
            for (int column = 0; column < Settings.BOARD_SIZE; column++) {
                if (Objects.isNull(Settings.INIT_BOARD[row][column]))
                    updateCell(null, row, column);
            }
        }
    }

}
