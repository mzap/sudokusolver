package pl.mzap.model;

import pl.mzap.exceptions.SegmentNotFoundException;

import java.util.Arrays;
import java.util.List;

public enum BoardSegment {

    TOP_LEFT(1, 1),
    TOP(1, 4),
    TOP_RIGHT(1, 7),
    LEFT(4, 1),
    CENTER(4, 4),
    RIGHT(4, 7),
    BOTTOM_LEFT(7, 1),
    BOTTOM(7, 4),
    BOTTOM_RIGHT(7, 7);

    private final int centerRowPosition;
    private final int centerColumnPosition;

    BoardSegment(int centerRowPosition, int centerColumnPosition) {
        this.centerRowPosition = centerRowPosition;
        this.centerColumnPosition = centerColumnPosition;
    }

    public static BoardSegment findActiveSegment(int row, int column) {
        return Arrays.stream(values())
                .filter(segmentRow -> getSegmentRows(segmentRow).contains(row))
                .filter(segmentCol -> getSegmentCols(segmentCol).contains(column))
                .findFirst()
                .orElseThrow(SegmentNotFoundException::new);
    }

    private static List<Integer> getSegmentRows(BoardSegment segment) {
        return List.of(segment.getRow() - 1, segment.getRow(), segment.getRow() + 1);
    }

    private static List<Integer> getSegmentCols(BoardSegment segment) {
        return List.of(segment.getColumn() - 1, segment.getColumn(), segment.getColumn() + 1);
    }

    public int getRow() {
        return centerRowPosition;
    }

    public int getColumn() {
        return centerColumnPosition;
    }
}
