package pl.mzap.model;

import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

import java.util.Objects;

public class Cell extends StackPane {

    private Byte number;
    private Label label;

    public Cell(Byte number, int row, int column) {
        this.number = number;
        this.label = new Label();
        if (Objects.nonNull(number)) {
            this.label.setText(String.valueOf(number));
        }
        getChildren().add(label);
        setStyle(row, column);
    }

    private void setStyle(int row, int column) {
        this.label.getStyleClass().add("label");
        getStyleClass().add("cell");
        if (column == 2 || column == 5)
            getStyleClass().add("right-end-cell");
        if (row == 2 || row == 5)
            getStyleClass().add("bottom-end-cell");
        if((column == 2 && row == 2) || (column == 5 && row == 5) || (column == 2 && row == 5) || (column == 5 && row == 2))
            getStyleClass().add("right-bottom-end-cell");
    }

    public Byte getNumber() {
        return number;
    }

    public void setNumber(Byte number) {
        this.number = number;
        if (Objects.nonNull(number)) {
            this.label.setText(String.valueOf(number));
            this.label.getStyleClass().add("label-new");
        } else {
            this.label.getStyleClass().add("label");
            this.label.setText("");
        }
    }

    @Override
    public String toString() {
        if (Objects.isNull(number))
            return "0";
        else
            return String.valueOf(number);
    }

}
