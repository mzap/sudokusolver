package pl.mzap.model;

import javafx.application.Platform;
import pl.mzap.Settings;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class Board {

    private final Byte[][] board = new Byte[Settings.BOARD_SIZE][];
    private final BoardView boardView;

    public Board(Byte[][] initBoard, BoardView boardView) {
        for (int row = 0; row < Settings.BOARD_SIZE; row++) {
            board[row] = Arrays.copyOf(initBoard[row], initBoard[row].length);
        }
        this.boardView = boardView;
    }

    public boolean calculate() {
        for (int row = 0; row < Settings.BOARD_SIZE; row++) {
            for (int column = 0; column < Settings.BOARD_SIZE; column++) {
                Byte number = board[row][column];
                if (Objects.isNull(number)) {
                    return predicateNumber(row, column);
                }
            }
        }
        return false;
    }

    private boolean predicateNumber(int row, int column) {
        for (byte predicatedNumber = 1; predicatedNumber <= Settings.BOARD_SIZE; predicatedNumber++) {
            BoardSegment activeSegment = BoardSegment.findActiveSegment(row, column);
            if (!isInGridColumn(predicatedNumber, column) && !isInGridRow(predicatedNumber, row) && !isInSegment(predicatedNumber, activeSegment)) {
                board[row][column] = predicatedNumber;
                updateBoardCellView(predicatedNumber, row, column);
                if (isResolved())
                    return true;
                else if (calculate())
                    return true;
                board[row][column] = null;
                updateBoardCellView(null, row, column);
            }
        }
        return false;
    }

    public void clearBoard() {
        for (int row = 0; row < Settings.BOARD_SIZE; row++) {
            board[row] = Arrays.copyOf(Settings.INIT_BOARD[row], Settings.INIT_BOARD[row].length);
        }
        if (Objects.nonNull(boardView)) {
            boardView.clearBoardView();
        }
    }

    private void updateBoardCellView(Byte number, int row, int column) {
        if (Objects.nonNull(boardView)) {
            Platform.runLater(() -> boardView.updateCell(number, row, column));
            if (Settings.TIME_INTERVAL_BETWEEN_MOVES > 0)
                try {
                    Thread.sleep(Settings.TIME_INTERVAL_BETWEEN_MOVES);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        }
    }

    public boolean isInSegment(byte predicatedNumber, BoardSegment segment) {
        for (int row = segment.getRow() - 1; row <= segment.getRow() + 1; row++) {
            for (int column = segment.getColumn() - 1; column <= segment.getColumn() + 1; column++) {
                Byte number = board[row][column];
                if (Objects.nonNull(number) && number.equals(predicatedNumber)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isInGridRow(byte predicatedNumber, int row) {
        for (int column = 0; column < Settings.BOARD_SIZE; column++) {
            Byte number = board[row][column];
            if (Objects.nonNull(number) && number.equals(predicatedNumber)) {
                return true;
            }
        }
        return false;
    }

    public boolean isInGridColumn(byte predicatedNumber, int column) {
        for (int row = 0; row < Settings.BOARD_SIZE; row++) {
            Byte number = board[row][column];
            if (Objects.nonNull(number) && number.equals(predicatedNumber)) {
                return true;
            }
        }
        return false;
    }

    private boolean isResolved() {
        for (int row = 0; row < Settings.BOARD_SIZE; row++) {
            for (int column = 0; column < Settings.BOARD_SIZE; column++) {
                Byte cellNumber = board[row][column];
                if (Objects.isNull(cellNumber))
                    return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder tabBuilder = new StringBuilder();
        for (int row = 0; row < Settings.BOARD_SIZE; row++) {
            for (int column = 0; column < Settings.BOARD_SIZE; column++) {
                tabBuilder.append("[");
                Optional.ofNullable(board[row][column])
                        .ifPresentOrElse(tabBuilder::append,
                                () -> tabBuilder.append(" "));
                tabBuilder.append("]");
            }
            tabBuilder.append("\n");
        }
        return tabBuilder.toString();
    }
}
