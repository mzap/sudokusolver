package pl.mzap;

public class Settings {

    public static final String APP_NAME = "Sudoku";
    public static final int BOARD_SIZE = 9;
    //    public static final Byte[][] INIT_BOARD = {
//            {null, null, null, 2, 6, null, 7, null, 1},
//            {6, 8, null, null, 7, null, null, 9, null},
//            {1, 9, null, null, null, 4, 5, null, null},
//            {8, 2, null, 1, null, null, null, 4, null},
//            {null, null, 4, 6, null, 2, 9, null, null},
//            {null, 5, null, null, null, 3, null, 2, 8},
//            {null, null, 9, 3, null, null, null, 7, 4},
//            {null, 4, null, null, 5, null, null, 3, 6},
//            {7, null, 3, null, 1, 8, null, null, null},
//    };
    public static final Byte[][] INIT_BOARD = {
            {null, 1, null, null, null, 8, null, null, 6},
            {null, null, null, null, null, null, null, 4, null},
            {6, 9, 5, null, null, null, null, null, 7},
            {null, 3, null, null, 1, 2, null, null, null},
            {null, null, null, 7, 9, 4, null, null, null},
            {null, null, 7, 5, 8, null, null, 2, null},
            {2, null, null, null, null, null, 5, 6, 8},
            {null, 7, null, null, null, null, null, null, null},
            {5, null, null, 2, null, null, null, 9, null},
    };
    public static final int TIME_INTERVAL_BETWEEN_MOVES = 1;

}
